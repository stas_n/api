== README

* Ruby version-2.4.1

* Rails version- 5


* Configuration

 bundle install

 rake db:create db:migrate

* How to run the test suite

   rake db:test:prepare

 rspec

* Usage

select user from Db

use name & email

go to http://localhost:3000/api/v1/get_token?name=my_name&email=mymail@mail.com

to obtain token for auth

then use this token for HTTPAuthorization

after it you can use app

rake grape:routes - shows all possible routes

