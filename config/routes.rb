Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'
  mount API::Root => '/api'
  mount GrapeSwaggerRails::Engine => '/swagger'
end
