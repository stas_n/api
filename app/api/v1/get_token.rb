module API
  module V1
    class GetToken < Grape::API
      format :json

      resource :get_token do
        desc 'Get token'
        params do
          requires :name, type: String, desc: 'User name'
          requires :email, type: String, desc: 'User email'
        end
        get '/' do
          User.generate_token(params)
        end
      end
    end
  end
end
