module API
  module V1
    module Defaults
      extend ActiveSupport::Concern

      included do
        version 'v1'
        format :json

        helpers do
          def current_user
            @current_user ||= User.authorize!(env)
          end

          def authenticate!
            return error!('401 Unauthorized', 401, 'X-Error-Detail' => 'Invalid token.') unless current_user
          end
        end

        before do
          authenticate! unless Rails.env.test?
        end
      end
    end
  end
end
