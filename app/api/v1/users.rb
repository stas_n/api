module API
  module V1
    class Users < Grape::API
      include API::V1::Defaults

      format :json
      resource :users do
        helpers do
          def model_class
            User
          end

          def model_entity
            API::V1::Entities::UserEntity
          end
        end

        desc 'List of all User by any type'
        include API::V1::Entities::Requests::GetAllRequest

        desc 'Search for records'
        get 'search' do
          data = model_class.user_posts_count(params[:search][:posts_count], params[:search][:date]).order('id DESC') if params[:search]
          present data, with: API::V1::Entities::UserEntity
        end

        desc 'Create new record'
        include API::V1::Entities::Requests::PostNewRecordRequest

        desc 'Modify single record'
        include API::V1::Entities::Requests::PutToModifyRecordRequest

        desc 'Delete single record'
        include API::V1::Entities::Requests::DeleteSingleRecordRequest

        desc 'Get single record by ID'
        include API::V1::Entities::Requests::GetSingleRecordRequest
      end
    end
  end
end
