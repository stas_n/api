module API
  module V1
    module Entities
      class DeletedRecordEntity < Grape::Entity
        expose :success
      end
    end
  end
end
