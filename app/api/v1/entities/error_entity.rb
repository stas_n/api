module API
  module V1
    module Entities
      class ErrorEntity < Grape::Entity
        expose :errors
      end
    end
  end
end
