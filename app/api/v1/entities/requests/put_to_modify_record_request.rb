module API
  module V1
    module Entities
      module Requests
        # module for modifying single record by PUT :id
        module PutToModifyRecordRequest
          extend ActiveSupport::Concern

          included do
            params do
              requires :id, type: Integer, desc: 'Model id'
            end
            put ':id' do
              model = model_class.find_by_id(params[:id])
              change_params = params.to_hash.symbolize_keys
              change_params.delete(:id)

              model.update_attributes(change_params)

              if model.valid?
                present model, with: model_entity
              else
                status 422
                response = { errors: model.errors.messages }
                present response, with: API::V1::Entities::ErrorEntity
              end
            end
          end
        end
      end
    end
  end
end
