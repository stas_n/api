module API
  module V1
    module Entities
      module Requests
        module PostNewRecordRequest
          extend ActiveSupport::Concern

          included do
            post '/' do
              add_params = params.to_hash.symbolize_keys
              model = model_class.create(add_params)

              if model.valid?
                present model, with: model_entity
              else
                status 422
                response = { errors: model.errors.messages }
                present response, with: API::V1::Entities::ErrorEntity
              end
            end
          end
        end
      end
    end
  end
end
