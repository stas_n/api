module API
  module V1
    module Entities
      module Requests
        module GetAllRequest
          extend ActiveSupport::Concern

          included do
            get '/' do
              sort_field = params[:sort_by] || 'id'
              raise 'wrong sort field' unless model_class.column_names.include?(sort_field)
              order = "#{sort_field} DESC"
              order = "#{sort_field} ASC" if params[:order] == 'ASC'

              data = model_class.ransack(params[:search]).result.order(order).all

              present (data), with: model_entity
            end
          end
        end
      end
    end
  end
end
