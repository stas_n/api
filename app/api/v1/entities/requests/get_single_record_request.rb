module API
  module V1
    module Entities
      module Requests
        module GetSingleRecordRequest
          extend ActiveSupport::Concern

          included do
            params do
              requires :id, type: Integer, desc: 'Model id'
            end
            get ':id' do
              record = model_class.find_by_id(params[:id])
              present record, with: model_entity
            end
          end
        end
      end
    end
  end
end
