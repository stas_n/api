module API
  module V1
    module Entities
      module Requests
        module DeleteSingleRecordRequest
          extend ActiveSupport::Concern

          included do
            params do
              requires :id, type: Integer, desc: 'Model id'
            end
            delete ':id' do
              success = false
              model_class.find_by_id(params[:id]).tap do |record|
                success = record.present?
                record.destroy if success
              end

              response = { success: success }
              present response, with: API::V1::Entities::DeletedRecordEntity
            end
          end
        end
      end
    end
  end
end
