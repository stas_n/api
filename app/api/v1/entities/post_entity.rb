module API
  module V1
    module Entities
      class PostEntity < Grape::Entity
        format_with(:iso_timestamp) { |dt| dt.iso8601 }

        expose :id, :name, :body, :user
        with_options(format_with: :iso_timestamp) do
          expose :created_at
          expose :updated_at
          expose :published_at
        end
      end
    end
  end
end
