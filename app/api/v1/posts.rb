module API
  module V1
    class Posts < Grape::API
      include API::V1::Defaults

      format :json
      resource :posts do
        helpers do
          def model_class
            Post
          end

          def model_entity
            API::V1::Entities::PostEntity
          end
        end

        desc 'List of all Post by any type'
        include API::V1::Entities::Requests::GetAllRequest

        desc 'Search for records'
        get 'search' do
          data = model_class.ransack(params[:search]).result.order('id DESC')
          present data, with: API::V1::Entities::PostEntity
        end

        desc 'Create new record'
        include API::V1::Entities::Requests::PostNewRecordRequest

        desc 'Modify single record'
        include API::V1::Entities::Requests::PutToModifyRecordRequest

        desc 'Delete single record'
        include API::V1::Entities::Requests::DeleteSingleRecordRequest

        desc 'Get single record by ID'
        include API::V1::Entities::Requests::GetSingleRecordRequest
      end
    end
  end
end
