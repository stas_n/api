require 'grape-swagger'
module API
  module V1
    class Root < Grape::API
      version 'v1', using: :path

      mount API::V1::Users
      mount API::V1::Posts
      mount API::V1::GetToken
      add_swagger_documentation
    end
  end
end
