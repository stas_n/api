class Post < ApplicationRecord
  belongs_to :user

  validates :name, :body, :published_at, presence: true
end
