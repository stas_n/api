class User < ApplicationRecord
  HMAC_SECRET = 'my$ecretK3y'

  has_many :posts

  validates :name, :email, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }

  def self.authorize!(env)
    return false unless env['HTTP_AUTHORIZATION']
    token = env['HTTP_AUTHORIZATION']

    begin
      params = (JWT.decode token, HMAC_SECRET, true, algorithm: 'HS256').first
      User.find_by(params).present?
    rescue JWT::DecodeError
      false
    end
  end

  def self.generate_token(params)
    JWT.encode params, HMAC_SECRET, 'HS256'
  end

  def self.user_posts_count(post_count, date = nil)
    query = select('users.*, count(posts.id)')
            .joins(:posts).group('users.id')
            .having("count(posts.id) >= #{post_count}")

    if date
      query.where('DATE(posts.created_at) = ?', date)
    else
      query
    end
  end

  def for_token
    "name=#{self.name}&email=#{self.email}"
  end
end
