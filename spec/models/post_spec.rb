require 'spec_helper'

describe Post do
  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:body) }
    it { should validate_presence_of(:published_at) }
  end

  context 'associations' do
    it { should belong_to(:user) }
  end
end
