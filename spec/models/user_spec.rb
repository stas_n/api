require 'spec_helper'

describe User do
  let(:user) { create(:user) }

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
  end

  context 'associations' do
    it { should have_many(:posts) }
  end

  context 'valid obj' do
    it { expect(user).to be_valid }
  end

  context "user's posts" do
    let!(:user) { create(:user_with_posts, posts_count: 10) }
    let!(:user1) { create(:user_with_posts, posts_count: 2) }
    it 'posts count' do
      expect(described_class.user_posts_count(1).to_a.size).to eq(2)
      expect(described_class.user_posts_count(10).to_a.size).to eq(1)
    end

    it 'posts count with date' do
      create(:post, user_id: user.id, created_at: Date.yesterday)
      expect(described_class.user_posts_count(1, Date.yesterday).to_a.size).to eq(1)
    end
  end

  it 'generate token' do
    user = create(:user, name: 'name', email: 'email@mail.co')
    expect(described_class.generate_token(email: user.email, name: user.name)).
        to eq('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImVtYWlsQG1haWwuY28iLCJuYW1lIjoibmFtZSJ9.bg53MlXQbV8qhcprIParvlMlf0J7xwWu_C5NFlHSK8k')
  end
end
