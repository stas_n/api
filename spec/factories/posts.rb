FactoryGirl.define do
  factory :post do
    name Faker::Team.creature
    body Faker::Lorem.paragraph
    published_at Faker::Time.between(DateTime.now - 1, DateTime.now)
    user
  end
end
