FactoryGirl.define do
  factory :user do
    email Faker::Internet.email
    name Faker::Name.name
  end

  factory :user_with_posts, parent: :user do
    transient do
      posts_count 5
    end
    after(:create) do |user, evaluator|
      create_list(:post, evaluator.posts_count, user: user)
    end
  end
end
