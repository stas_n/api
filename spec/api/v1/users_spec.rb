require 'spec_helper'

describe API::V1::User do
  describe do
    let!(:user) { create(:user) }

    context 'HTTP request to User API' do
      let(:collection) { User.all }

      it 'get users list' do
        get '/api/v1/users'
        expect(collection.count).to eq(1)
      end

      # it 'create post' do
      #   binding.pry
      #   params = { name: 'name', body: 'body', published_at: Date.today }
      #   post 'api/v1/users', params: params
      #   post :create, params: { post: { name: 'name', body: 'body', published_at: Date.today } }
      #   post 'api/v1/users', params: {post: { title: "First post", body: "This is the body"} }
      #
      #   expect(response.status).to eq 201
      #   expect(collection.count).to eq(2)
      # end

      it 'update user' do
        user_id = User.last.id
        params = { name: 'ivan' }
        put "/api/v1/users/#{user_id}", params: params

        expect(response.status).to eq 200
        expect((JSON.parse(response.body)['name'])).to eq('ivan')
      end

      it 'delete user' do
        user_id = User.last.id
        delete "/api/v1/users/#{user_id}"
        expect(response.status).to eq 200
        expect(collection.count).to eq(0)
      end
    end

    context 'filters if params[:search] specified' do
      it 'filter with specific params' do
        create(:user_with_posts)
        get('/api/v1/users/search', params: { search: { posts_count: 1, date: Date.today } })
        expect(response.status).to eq 200
        expect(JSON.parse(response.body).count).to eq 1
      end
    end
  end
end
