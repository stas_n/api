require 'spec_helper'

describe API::V1::Posts do
  describe do
    let!(:post) { create(:post) }

    context 'HTTP request to Post API' do
      let(:collection) { Post.all }

      it 'get posts list' do
        get '/api/v1/posts'
        expect(collection.count).to eq(1)
      end

      it 'update post' do
        post_id = Post.last.id
        params = { name: 'post' }
        put "/api/v1/posts/#{post_id}", params: params

        expect(response.status).to eq 200
        expect((JSON.parse(response.body)['name'])).to eq('post')
      end

      it 'delete post' do
        post_id = Post.last.id
        delete "/api/v1/posts/#{post_id}"
        expect(response.status).to eq 200
        expect(collection.count).to eq(0)
      end
    end

    context 'filters if params[:search] specified' do
      it 'filter with specific name&body params' do
        create(:post, name: 'name', body: 'body')
        get('/api/v1/posts/search', params: { search: { name_cont: 'name', body_cont: 'body' } })
        expect(response.status).to eq 200
        expect(JSON.parse(response.body).count).to eq 1
      end

      it 'filter with specific user params' do
        user = create(:user)
        create(:post, name: 'name', body: 'body', user: user)
        get('/api/v1/posts/search', params: { search: { user_id_eq: user.id, user_email_cont: user.email } })
        expect(response.status).to eq 200
        expect(JSON.parse(response.body).count).to eq 1
      end
    end
  end
end
