require 'spec_helper'

describe API::V1::Posts do
  describe do
    context 'Get token' do
      it 'token generation' do
        get '/api/v1/get_token', params: { name: 'name', email: 'email' }
        expect(response.status).to eq 200
        expect((JSON.parse(response.body))).to eq('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoibmFtZSIsImVtYWlsIjoiZW1haWwifQ.VyCwM5CNjBnvTw7XOY3Ms3tfN7KQ_T4deXU42gyCJS4')
      end
    end
  end
end
