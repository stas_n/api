class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :name
      t.text :body
      t.integer :user_id
      t.datetime :published_at

      t.timestamps
    end
  end
end
