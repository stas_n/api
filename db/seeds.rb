5.times { User.create(name: Faker::Name.name, email: Faker::Internet.email) }

User.all.each do |user|
  rand(10).times {
    user.posts.create(name: Faker::Team.creature, body: Faker::Lorem.paragraph,
                      published_at: Faker::Time.between(DateTime.now - 10, DateTime.now))
  }
end